resource "aws_s3_bucket" "cache" {
  bucket = var.gitlab_runner_cache_name

  tags = merge(local.service_tags,
    { "Name" : var.gitlab_runner_cache_name,
  "Comments" : "Shared cache for Docker Machines created by gitlab-runner" })
}

resource "aws_s3_bucket_acl" "cache_bucket_acl" {
  bucket = aws_s3_bucket.cache.id
  acl    = "private"
}

resource "aws_s3_bucket_ownership_controls" "runner_bucket_controls" {
  bucket = aws_s3_bucket.cache.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}
