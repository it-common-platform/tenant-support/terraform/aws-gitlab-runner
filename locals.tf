locals {
  service_tags = {
    Name              = "gitlab-runner-supervisor-${var.service_name}"
    Service           = var.service_name
    Environment       = var.environment
    ResponsibleParty  = var.responsible_party
    ResponsibleParty2 = var.responsible_party2
    DataRisk          = var.data_risk
    ComplianceRisk    = var.compliance_risk
    Documentation     = var.documentation
    Comments          = "Gitlab runner for ${var.service_name}/${var.environment}"
    VCS               = var.vcs
  }
}