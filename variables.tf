###
# Required
###
variable "gitlab_runner_cache_name" {
  description = "S3 bucket name to be used for the gitlab runner cache"
}

variable "responsible_party" {
  description = "Person (pid) who is primarily responsible for the configuration and maintenance of this resource"
}

variable "ssh_key_name" {
  description = "Name of the SSH key you want to use to access the bastion"
}

variable "subnet_ids" {
  description = "List of subnet ids in which to create resources"
  type        = list(any)
}

variable "vcs" {
  description = "A link to the repo in a version control system (usually Git) that manages this resource."
}

variable "vpc_id" {
  description = "The ID of the VPC we should use for EC2 instances"
}

###
# Optional
###

variable "ami_filter" {
  description = "name filter for EC2 AMI"
  default     = "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-????????"
}

variable "ami_owner" {
  description = "Owner of the EC2 AMI"
  default     = "099720109477" # Canonical
}

variable "ami_user" {
  description = "default user for the EC2 AMI"
  default     = "ubuntu"
}

variable "aws_region" {
  description = "AWS region to run in"
  default     = "us-east-1"
}

variable "compliance_risk" {
  description = "Should be `none`, `ferpa` or `pii`"
  default     = "none"
}

variable "data_risk" {
  description = "Should be `low`, `medium` or `high` based on data-risk classifications defined in VT IT Policies"
  default     = "low"
}

variable "documentation" {
  description = "Link to documentation and/or history file"
  default     = "none"
}

variable "environment" {
  description = "e.g. `development`, `test`, or `production`"
  default     = "build"
}

variable "gitlab_runner_version" {
  description = "version string for the gitlab runner to install"
  default     = "latest"
}

variable "gitlab_config_secret_id" {
  description = "AWS Secrets Manager secret id for secret containing your complete gitlab runner config"
  default     = "none"
}

variable "gitlab_token" {
  description = "Gitlab token for your group or project"
  default     = "none"
}

variable "gitlab_url" {
  description = "URL to the Gitlab instance the runner will serve"
  default     = "https://code.vt.edu/"
}

variable "instance_type" {
  description = "The EC2 instance type to use for the docker machines"
  default     = "m4.large"
}

variable "log_retention" {
  description = "Number of days to keep logs from Docker Machines created by gitlab-runner"
  default     = 14
}

variable "machine_iam_instance_profile" {
  description = "Name of an AWS IAM role name to assign as the profile for new instances"
  default     = ""
}

variable "supervisor_iam_profile_arn" {
  description = "ARN of the role the new priviledged supervisor"
  default     = ""
}

variable "machine_idle_nodes" {
  description = "Maximum idle machines"
  default     = 2
}

variable "machine_idle_time" {
  description = "Minimum time after node can be destroyed"
  default     = 3600
}

variable "machine_max_builds" {
  description = "Maximum number of builds processed by machine"
  default     = 50
}

variable "maximum_concurrent_jobs" {
  description = "Maximum amount of cuncurrent jobs that can be run globally"
  default     = 2
}

variable "maximum_nodes_limit" {
  description = "Maximum number of runners that could be spun up"
  default     = 2
}

variable "responsible_party2" {
  description = "Backup for responsible_party"
  default     = "none"
}

variable "runner_instance_type" {
  description = "Instance type for Gitlab Runner"
  default     = "t3a.small"
}

variable "runner_name" {
  description = "The runner name as seen in Gitlab"
  default     = "gitlab-runner"
}

variable "runner_tags" {
  description = "Tags to apply to the GitLab Runner, comma-separated"
  default     = ""
}

variable "runner_maximum_spot_price" {
  description = "Maximum bid price for SPOT instances used for runners"
  default     = ""
}

variable "runners_use_private_address_only" {
  description = "Gitlab Runners use Private IP Addresses only"
  default     = false
}

variable "service_name" {
  description = "The high level service this resource is primarily supporting"
  default     = "build"
}

variable "use_public_ip_for_bastion" {
  description = "set true if you want a public ip allocated for the bastion host"
  default     = "false"
}

variable "use_spot" {
  description = "Enables using SPOT instances for runners"
  default     = "false"
}
